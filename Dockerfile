FROM httpd
RUN apt-get update && apt-get install -y \
    && rm -rf /var/lib/apt/lists/*
COPY httpd.conf /usr/local/apache2/conf/httpd.conf
